# xmpp2bbcode

xmpp2bbcode is a program that converts from XMPP input into BBCode output.

## Installation
1. In a Debian-based distribution, install the dependencies with:
   ```bash
   sudo apt install python3
   ```
2. Download the [script](https://codeberg.org/ghjardim/xmpp2bbcode/raw/branch/main/xmpp2bbcode).
3. Make it executable: `chmod +x /path/to/xmpp2bbcode`
4. Place it somewhere in your `$PATH`, for example, by doing `mv /path/to/xmpp2bbcode ~/.local/bin/`

## Usage
```
usage: xmpp2bbcode [-h] -i <input_file> [-o <output_file>] -t <pattern> [-v]

Converts from XMPP input into BBCode output.

optional arguments:
  -h, --help            show this help message and exit
  -i <input_file>, --input <input_file>
                        input file to parse, containing text pasted from any
                        supported XMPP client.
  -o <output_file>, --output <output_file>
                        output file to write parsed string. If ommited, prints
                        the parsed file
  -t <pattern>, --timestamp-pattern <pattern>
                        timestamp pattern. Example: "(00:00) "
  -v, --version         show version and exit

Supported clients are: Gajim, Pidgin.
```

## Contribution
Feel free to contribute with issues and pull requests!
